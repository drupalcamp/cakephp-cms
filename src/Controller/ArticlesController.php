<?php
// src/Controller/ArticlesController.php

namespace App\Controller;



class ArticlesController extends AppController
{
  //  http://cakecms.test/articles/index
  public function index()
  {
      $this->loadComponent('Paginator');
      
      $articles = $this->Paginator->paginate($this->Articles->find());

      $this->Authorization->skipAuthorization();
      $this->set(compact('articles'));

  }

  public function view($slug = null) {
    $article = $this->Articles
    ->findBySlug($slug)
    ->contain('Tags')
    ->firstOrFail();

    $this->Authorization->skipAuthorization();

    $this->set(compact('article'));

  }


  public function add()
  {
      $article = $this->Articles->newEmptyEntity();

      $this->Authorization->authorize($article);

      if ($this->request->is('post')) {
          $article = $this->Articles->patchEntity($article, $this->request->getData());

          // Hardcoding the user_id is temporary, and will be removed later
          // when we build authentication out.
          // $article->user_id = 1;
        // Changed: Set the user_id from the current user.
        $article->user_id = $this->request->getAttribute('identity')->getIdentifier();

          if ($this->Articles->save($article)) {
              $this->Flash->success(__('Your article has been saved.'));
              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('Unable to add your article.'));
      }
      
      // Get a list of tags.
      $tags = $this->Articles->Tags->find('list')->all();
      // Set tags to the view context
      $this->set('tags', $tags);

      $this->set('article', $article);
  }


  public function edit($slug) {

    $article = $this->Articles
        ->findBySlug($slug)
        ->contain('Tags')
        ->firstOrFail();

    $this->Authorization->authorize($article);

    if ($this->request->is(['post', 'put'])) {
        $this->Articles->patchEntity($article, $this->request->getData(), 
          ['accessibleFields' => ['user_id' => false]]);

        if ($this->Articles->save($article)) {
            $this->Flash->success(__('Your article has been updated.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Unable to update your article.'));
    }
    
    $this->set('articleTest', $article);
    // Get a list of tags.
    $tags = $this->Articles->Tags->find('list')->all();
    // Set tags to the view context
    $this->set('tags', $tags);
    
  }

  public function delete($slug)
  {
      $this->request->allowMethod(['post', 'delete']);

      $article = $this->Articles
          ->findBySlug($slug)
          ->firstOrFail();

      $this->Authorization->authorize($article);

      if ($this->Articles->delete($article)) {
          $this->Flash->success(__('The {0} article has been deleted.', $article->title));
          return $this->redirect(['action' => 'index']);
      }
  }

  // $builder->scope('/articles', function (RouteBuilder $builder) {
  //   $builder->connect('/tagged/*', ['controller' => 'Articles', 'action' => 'tags']);
  // });
  public function tags()
  { 
    // The 'pass' key is provided by CakePHP and contains all
    // the passed URL path segments in the request.
    $tags = $this->request->getParam('pass');

    $this->Authorization->skipAuthorization();

    // Use the ArticlesTable to find tagged articles.
    $articles = $this->Articles->find('tagged', [
            'tags' => $tags
        ])
        ->all();

    // Pass variables into the view template context.
    $this->set([
        'articles' => $articles,
        'tags' => $tags
    ]);
  }


    // in src/Controller/AppController.php
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // for all controllers in our application, make index and view
        // actions public, skipping the authentication check
        $this->Authentication->addUnauthenticatedActions(['index', 'view']);
    }
 

}